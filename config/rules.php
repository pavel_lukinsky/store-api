<?php 

/**
 * Routing rules
 * 
 * @var array
 */
return [   
    '<controller:>/<action:>' => '<controller>/<action>',
    '<controller:(product)>/<action:>/<product_id:\d+>/<category_id:\d+>' => '<controller>/<action>/',
    [
        'pattern' => '<controller:>/<action:>/<id:\d+>/<offset:\d+>',
        'route' => '<controller>/<action>',
        'defaults' => ['offset' => 0],
    ]
]; 