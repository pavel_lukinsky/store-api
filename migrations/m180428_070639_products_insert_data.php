<?php

use yii\db\Migration;

/**
 * Class m180428_070639_products_insert_data
 */
class m180428_070639_products_insert_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('products', [
            'name' => 'Продукт 1',
            'description' => 'Описание продукта 1',
        ]);
        
        $this->insert('products', [
            'name' => 'Продукт 2',
            'description' => 'Описание продукта 2',
        ]);
        
        $this->insert('products', [
            'name' => 'Продукт 3',
            'description' => 'Описание продукта 3',
        ]);
        
        $this->insert('products', [
            'name' => 'Продукт 4',
            'description' => 'Описание продукта 4',
        ]);
        
        $this->insert('products', [
            'name' => 'Продукт 5',
            'description' => 'Описание продукта 5',
        ]);
        
        $this->insert('products', [
            'name' => 'Продукт 6',
            'description' => 'Описание продукта 6',
        ]);
        
        $this->insert('products', [
            'name' => 'Продукт 7',
            'description' => 'Описание продукта 7',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180428_070639_products_insert_data cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180428_070639_products_insert_data cannot be reverted.\n";

        return false;
    }
    */
}
