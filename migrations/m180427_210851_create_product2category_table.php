<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product2category`.
 */
class m180427_210851_create_product2category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('product2category', [
            'product_id' => $this->integer(),
            'category_id' => $this->integer()
        ]);
        
        $this->createIndex(
            'idx-product2category-product_id',
            'product2category',
            'product_id'
        );
        
        $this->addForeignKey(
            'fk-product2category-product_id',
            'product2category',
            'product_id',
            'products',
            'id',
            'CASCADE'
        );
        
        $this->createIndex(
            'idx-product2category-category_id',
            'product2category',
            'category_id'
        );
        
        $this->addForeignKey(
            'fk-product2category-category_id',
            'product2category',
            'category_id',
            'categories',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-product2category-product_id',
            'product2category'
        );
        
        $this->dropIndex(
            'idx-product2category-product_id',
            'product2category'
        );
        
        $this->dropForeignKey(
            'fk-product2category-category_id',
            'product2category'
        );
        
        $this->dropIndex(
            'idx-product2category-category_id',
            'product2category'
        );
        
        $this->dropTable('product2category');
    }
}
