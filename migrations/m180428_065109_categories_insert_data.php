<?php

use yii\db\Migration;

/**
 * Class m180428_065109_categories_insert_data
 */
class m180428_065109_categories_insert_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('categories', [
            'name' => 'Категория 1',
            'description' => 'Описание категории 1',
        ]);
        
        $this->insert('categories', [
            'name' => 'Категория 2',
            'description' => 'Описание категории 2',
        ]);
        
        $this->insert('categories', [
            'name' => 'Категория 3',
            'description' => 'Описание категории 3',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180428_065109_categories_insert_data cannot be reverted.\n";

        return false;
    }
}
