<?php

use yii\db\Migration;

/**
 * Class m180428_070816_product2category_insert_data
 */
class m180428_070816_product2category_insert_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('product2category', [
            'product_id' => 1,
            'category_id' => 1,
        ]);
        
        $this->insert('product2category', [
            'product_id' => 2,
            'category_id' => 1,
        ]);
        
        $this->insert('product2category', [
            'product_id' => 3,
            'category_id' => 2,
        ]);
        
        $this->insert('product2category', [
            'product_id' => 4,
            'category_id' => 2,
        ]);
        
        $this->insert('product2category', [
            'product_id' => 5,
            'category_id' => 3,
        ]);
        
        $this->insert('product2category', [
            'product_id' => 6,
            'category_id' => 3,
        ]);
        
        $this->insert('product2category', [
            'product_id' => 7,
            'category_id' => 1,
        ]);
        
        $this->insert('product2category', [
            'product_id' => 7,
            'category_id' => 3,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180428_070816_product2category_insert_data cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180428_070816_product2category_insert_data cannot be reverted.\n";

        return false;
    }
    */
}
