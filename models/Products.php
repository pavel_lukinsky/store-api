<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "products".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 *
 * @property Product2category[] $product2categories
 */
class Products extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'products';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'description' => 'Описание',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct2categories()
    {
        return $this->hasMany(Product2category::className(), ['product_id' => 'id']);
    }
    
    /**
     * Return product categories list
     * 
     * @return \app\models\Categories[]
     */
    public function getCategories()
    {
        $categories = [];
        foreach ($this->product2categories as $p2c) {
            $categories[] = $p2c->category;
        }
        
        return $categories;
    }
}
