<?php

namespace app\controllers;

use Yii;
use yii\web\Response;
use yii\web\Controller;

class ApiController extends Controller
{
    const UNKNOW_ERROR_CODE = 404;
    
    const NO_NAME_CODE = 100001;
    const NO_CATEGORY_CODE = 100002;
    const NO_PRODUCT_CODE = 100003;
    const NO_BINDING_CODE = 100004;
    
    protected $response;
    
    private $errors = [
        10001 => 'Category name is empty',
        10002 => 'Category does not exist'
    ];
    
    public function __construct($id, $module)
    {
        parent::__construct($id, $module);
        
        $this->response = Yii::$app->response;
        
        $this->response->format = Response::FORMAT_JSON;
        $this->response->statusCode = 200;
        
        $this->response->data = [
            'error' => 0,
            'message' => ''
        ];
        
    }
    
    protected function setErrorResponse($code)
    {
        if (!in_array($code, $this->errors)) {
            $this->response->data['error'] = self::UNKNOW_ERROR_CODE;
            $this->response->data['message'] = 'unknow error';
            
            return;
        }
        
        $this->response->data['error'] = $code;
        $this->response->data['message'] = $this->errors[$code];
    }
}
