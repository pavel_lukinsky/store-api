<?php

namespace app\controllers;

use Yii;
use app\models\Categories;
use app\models\Products;

class CategoryController extends ApiController
{
    /**
     * Default limit for products
     */
    const DEFAULT_LIMIT = 2;
    
    /**
     * Get categories list
     * 
     * @return \yii\web\Response|\yii\console\Response
     */
    public function actionList()
    {
        $categories = Categories::find()
            ->orderBy('name')
            ->all();
        
        $this->response->data['categories'] = $categories;
        
        return $this->response;
    }
    
    /**
     * Get products by category with pagination
     * 
     * @param integer $id
     * @param integer $offset
     * @return \yii\web\Response|\yii\console\Response
     */
    public function actionProducts(int $id, int $offset = 0)
    {        
        $products = Products::find()
            ->leftJoin('product2category', 'product2category.product_id = products.id')
            ->where(['product2category.category_id' => $id])
            ->orderBy('products.name')
            ->limit(self::DEFAULT_LIMIT)
            ->offset($offset)
            ->all();
        
        $this->response->data['products'] = $products;
            
        return $this->response;    
    }
    
    /**
     * Create category
     * 
     * @return \yii\web\Response|\yii\console\Response
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;

        if (!$request->post('name')) {
            $this->setErrorResponse(self::NO_NAME_CODE);
            
            return $this->response;
        }

        $category = new Categories();
        
        $category->name = $request->post('name');
        $category->description = $request->post('description');
        
        $category->save();
        
        return $this->response;
    }
    
    /**
     * Delete category
     * 
     * @param integer $id
     * @return \yii\web\Response|\yii\console\Response
     */
    public function actionDelete(int $id)
    {
        $category = Categories::findOne($id);
        
        if (!$category) {
            $this->setErrorResponse(self::NO_CATEGORY_CODE);
            
            return $this->response;
        }

        $category->delete();
        
        return $this->response;
    }
}
