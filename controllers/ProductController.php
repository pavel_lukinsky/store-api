<?php

namespace app\controllers;

use app\models\Products;
use app\models\Categories;
use app\models\Product2category;

class ProductController extends ApiController
{
    /**
     * Get product info
     * 
     * @param int $id
     * @return \yii\web\Response|\yii\console\Response
     */
    public function actionItem(int $id)
    {
        $product = Products::findOne($id);
        
        if (!$product) {
            $this->setErrorResponse(self::NO_PRODUCT_CODE);
            
            return $this->response;
        }
        
        $this->response->data['products'] = $product;
        $this->response->data['categories'] = $product->categories;
        
        return $this->response;
    }
    
    /**
     * Add product to category
     * 
     * @param integer $id
     * @param integer $category
     * @return \yii\web\Response|\yii\console\Response
     */
    public function actionAddToCategory(int $product_id, int $category_id)
    {
        if (!$this->checkProduct($product_id)) {
            $this->setErrorResponse(self::NO_PRODUCT_CODE);
            
            return $this->response;
        }
        
        if (!$this->checkCategory($category_id)) {
            $this->setErrorResponse(self::NO_CATEGORY_CODE);
            
            return $this->response;
        }
        
        if ($this->checkProduct2Category($product_id, $category_id)) {
            $this->setErrorResponse(self::NO_BINDING_CODE);
            
            return $this->response;
        }

        $p2c = new Product2category();
        
        $p2c->product_id = $product_id;
        $p2c->category_id = $category_id;
        
        $p2c->save();
        
        return $this->response;
    }
    
    /**
     * Delete product from category
     * 
     * @param int $product_id
     * @param int $category_id
     * @return \yii\web\Response|\yii\console\Response
     */
    public function actionDeleteFromCategory(int $product_id, int $category_id)
    {
        if (!$this->checkProduct($product_id)) {
            $this->setErrorResponse(self::NO_PRODUCT_CODE);
            
            return $this->response;
        }
        
        if (!$this->checkCategory($category_id)) {
            $this->setErrorResponse(self::NO_CATEGORY_CODE);
            
            return $this->response;
        }
       
        if (!$this->checkProduct2Category($product_id, $category_id)) {
            $this->setErrorResponse(self::NO_BINDING_CODE);
            
            return $this->response;
        }
        
        Product2category::deleteAll(['product_id' => $product_id, 'category_id' => $category_id]);
        
        return $this->response;
    }
    
    /**
     * Check product
     * 
     * @param int $id
     * @return boolean
     */
    private function checkProduct(int $id): bool
    {
        if (Products::findOne($id)) return true;

        return false;
    }
    
    /**
     * Check category
     * 
     * @param int $id
     * @return bool
     */
    private function checkCategory(int $id): bool
    {
        if (Categories::findOne($id)) return true;
        
        return false;
    }
    
    /**
     * Check relationship product to category
     * 
     * @param int $product_id
     * @param int $category_id
     * @return bool
     */
    private function checkProduct2Category(int $product_id, int $category_id): bool
    {
        if (Product2category::findOne(['product_id' => $product_id, 'category_id' => $category_id])) return true;
        
        return false;
    }
}
